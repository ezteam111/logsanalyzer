/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var host = window.location.protocol + '//' + window.location.host;
    var apiUrl = host + '/BugsBunny/api';
    var url = apiUrl + '/bugs/id/';
    tableCreate();
    function copyToPastebin(id, stackTrace) {
        $("#a-copy-" + id).click(
                function () {
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/paste",
                        dataType: 'text',
                        contentType: "text/plain; charset=utf-8",
                        cache: false,
                        data: stackTrace,
                        success: function (response) {
                            window.open(response);
                        }
                    });
                });
    }
    function tableCreate() {
        var id = Number($(".copy-to-haste").data("item-id"));
        $.getJSON(url + id, function (data) {
            $(".copy-to-haste").append(
                    '<a id="a-copy-' + id + '">Copy error to haste</a>')
            copyToPastebin(id, data.stackTrace)
        });

    }

});