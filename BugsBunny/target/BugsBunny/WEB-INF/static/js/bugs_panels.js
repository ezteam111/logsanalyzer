/* 
 * To change this license title, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    var host = window.location.protocol + '//' + window.location.host;

    var apiUrl = host + '/BugsBunny/api';

    var bugsUrl = apiUrl + '/bugs';
    var appsUrl = apiUrl + '/apps';
    var statusUrl = apiUrl + '/statuses';


    var bool = false;

    $.getJSON(appsUrl, function (appData) {
        if ($.cookie("apps") !== null) {
            for (var i in appData) {
                if ($.cookie("apps") === appData[i]) {
                    bool = true;
                    break;
                }
            }
        }
        if (($.cookie("apps") === null) || (bool === false)) {
            $.cookie("apps", appData[0], {expires: 1});
        }
    });

    function modifyStatusFromBackEnd(status) {
        switch (status) {
            case 'o':
                return 'Open';
            case 'b':
                return 'Bug';
            case 'n':
                return 'Not a bug';
            case 'd':
                return 'Duplicate';
        }
    }

    function modifyStatusToBackEnd(status) {
        switch (status) {
            case 'Open':
                return 'o';
            case 'Bug':
                return 'b';
            case 'Not a bug':
                return 'n';
            case 'Duplicate':
                return 'd';
        }
    }

    $.getJSON(statusUrl, function (statusData) {
        if ($.cookie("bug-status") !== null) {
            for (var i in statusData) {
                if ($.cookie("bug-status") === statusData[i]) {
                    bool = true;
                    break;
                }
            }
        }
        if (($.cookie("bug-status") === null) || (bool === false)) {
            $.cookie("bug-status", statusData[0], {expires: 1});
        }
    });

    var url = bugsUrl + '/appid/' + $.cookie("apps") + '?status=' + $.cookie("bug-status");

    $.getJSON(appsUrl)
            .then(sortAppDropdown)
            .then(sortStatusDropdown)
            .then(panelsCreation);



    function sortAppDropdown(data) {
        $.each(data, function (index, appId) {
            if (appId !== $.cookie("apps")) {
                $("#apps").append('\n\
                    <option data-item-id="' + appId + '">' + appId + '</option>');
            } else {
                $("#apps").append('\n\
                    <option data-item-id="' + appId + '" selected>' + appId + '</option>');
            }
        });
    }

    function sortStatusDropdown() {
        $.getJSON(statusUrl, function (data) {
            $.each(data, function (index, status) {
                if (status !== $.cookie("bug-status")) {
                    $("#bug-status").append('\n\
                    <option data-item-id="' + status + '">' + modifyStatusFromBackEnd(status) + '</option>');
                } else {
                    $("#bug-status").append('\n\
                    <option data-item-id="' + status + '" selected>' + modifyStatusFromBackEnd(status) + '</option>');
                }
            });
        });
    }

    $(function () {
        $("#apps").change(function () {
            $.cookie("apps", $(this).select().val(), {expires: 7});
            location.reload();
        });
    });

    $(function () {
        $("#bug-status").change(function () {
            $.cookie("bug-status", modifyStatusToBackEnd($(this).select().val()), {expires: 7});
            location.reload();
        });
    });

    function setBugStatus(id) {

        $("#save-order-" + id).click(function () {

            var editID = $(this).data('item-id');
            var myData;

            if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                myData = JSON.stringify({
                    status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                    mainBugId: Number($("#order-title-" + id + " option:selected").data("item-id"))
                });
            } else if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                myData = JSON.stringify({
                    status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                    taskUri: $("#bug-task-" + id).val()
                });
            } else {
                myData = JSON.stringify({
                    status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val())
                });
            }
            $.ajax({
                type: "PUT",
                url: bugsUrl + "/id/" + editID,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: myData,
                success: function () {
                }
            });
        });
    }

    function setDuplicate(id) {
        var titlesUrl = apiUrl + '/titles?appId=' + $.cookie("apps") + '&status=' + $.cookie("bug-status");
        $.getJSON(titlesUrl, function (data) {
            $.each(data, function (titleId, title) {
                if (Number(titleId) !== id) {
                    $("#order-title-" + id).append(
                            '<option data-item-id="' + Number(titleId) + '">' + title + '</option>'
                            );
                }
            });
        });
    }

    function show(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                        $("#order-title-" + id).css("visibility", "visible");
                    } else {
                        $("#order-title-" + id).css("visibility", "hidden");
                    }
                });
    }

    function setInputReadonly(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                        $("#bug-task-" + id).prop('readonly', false);
                    } else {
                        $("#bug-task-" + id).prop('readonly', true);
                    }
                });
    }

    function copyToPastebin(id, stackTrace) {
        $("#copy-" + id).click(
                function () {
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/paste",
                        dataType: 'text',
                        contentType: "text/plain; charset=utf-8",
                        cache: false,
                        data: stackTrace,
                        success: function (response) {
                            window.open(response);
                        }
                    });
                });
    }

    function panelsCreation() {
        $.getJSON(url, function (data) {
            $.each(data, function (index, bug) {
                var tmpCrDate = new Date(bug.creationDate);
                var creationDate = tmpCrDate.toISOString().substr(0, 10) + " " + tmpCrDate.toISOString().substr(11, 8);
                var tmpLSDate = new Date(bug.lastSeen);
                var lastSeen = tmpLSDate.toISOString().substr(0, 10) + " " + tmpLSDate.toISOString().substr(11, 8);
                $("#panel-table").append(
                        '<div class="panel panel-default clear-div">\n\
                        <div class="panel-heading">\n\
                            <div class="row">\n\
                                <div class="panel-title  text-center">\n\
                                    <div class="col-sm-2">' + bug.id + '</div>\n\
                                    <div class="col-sm-2">' + creationDate + '</div>\n\
                                    <div class="col-sm-2">' + modifyStatusFromBackEnd(bug.status) + '</div>\n\
                                    <div class="col-sm-2">' + bug.lastCount + '</div>\n\
                                    <div class="col-sm-2">' + lastSeen + '</div>\n\
                                    <a class="col-sm-1 pull-right" data-toggle="collapse" href="#collapse' + bug.id + '">Collapse</a>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                        <div id="collapse' + bug.id + '" class="panel-collapse collapse">\n\
                            <div class="panel-body" data-item-id="' + bug.hashCheckSum + '">\n\
                                <div class="row">\n\
                                    <div class="col-sm-offset-1 col-sm-10">\n\
                                        <p><strong>' + bug.title + '</strong   ></p>\n\
                                        <p><strong>RayID: ' + bug.rayID + '</strong></p>\n\
                                        <p id="stackTrace-' + bug.id + '">'
                        + bug.stackTrace.split("\n").join("<br>").split(" ").join("&nbsp;")
                        + '</p></div>\n\
                                    </div>\n\
                                </div>\n\
                            <div class="panel-footer">\n\
                                <form class="form-horizontal" role="decorate-form">\n\
                                    <div class="form-group ">\n\
                                        <label class="col-sm-2 control-label" for="bug-task-' + bug.id + '">Bug task</label>\n\
                                        <div class="col-sm-6">\n\
                                            <input type="text" class="form-control" id="bug-task-' + bug.id + '" readonly value="' + bug.taskUri + '">\n\
                                        </div>\n\
                                        <div class="col-sm-offset-2 col-sm-2">\n\
                                            <button type="button" class="btn btn-default" id="copy-' + bug.id + '">Copy error to haste</button>\n\
                                        </div>\n\
                                    </div>\n\
                                </form>\n\
                                <form class="form-horizontal" role="change-status">\n\
                                    <div class="form-group">\n\
                                        <label class="col-sm-2 control-label" for="order-status-' + bug.id + '">Update order status</label>\n\
                                        <div class="col-sm-2">\n\
                                            <select class="form-control select-status" id="order-status-' + bug.id + '">\n\
                                                <option>Open</option>\n\
                                                <option>Bug</option>\n\
                                                <option>Not a bug</option>\n\
                                                <option>Duplicate</option>\n\
                                            </select>\n\
                                        </div>\n\
                                        <div class="col-sm-2">\n\
                                            <select class="form-control select-title" id="order-title-' + bug.id + '" style="visibility:hidden;">\n\
                                                <option data-item-id="' + bug.id + '" selected disabled>' + bug.title + '</option>\n\
                                            </select>\n\
                                        </div>\n\
                                        <button type="submit" data-item-id="' + bug.id + '" id="save-order-' + bug.id + '" class="col-sm-1 btn btn-primary save-order">Save</button>\n\
                                    </div>\n\
                                </form>\n\
                            </div>\n\
                        </div>\n\
                    </div>'
                        );
                setDuplicate(bug.id);
                show(bug.id);
                setBugStatus(bug.id);
                setInputReadonly(bug.id);
                copyToPastebin(bug.id, bug.stackTrace);
            });
        });
    }
});