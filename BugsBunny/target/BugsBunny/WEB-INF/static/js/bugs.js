$(document).ready(function () {

    var host = window.location.protocol + '//' + window.location.host;
    var apiUrl = host + '/BugsBunny/api';
    var bugsUrl = apiUrl + '/bugs';

    var urlRegExp = /^https?:\/\/[a-z0-9./]+/i;

    function modifyStatusFromBackEnd(status) {
        switch (status) {
            case 'o':
                return 'Open';
            case 'b':
                return 'Bug';
            case 'n':
                return 'Not a bug';
            case 'd':
                return 'Duplicate';
            case 'a':
                return 'All';
            case 'f':
                return 'Fixed';
        }
    }

    function modifyStatusToBackEnd(status) {
        switch (status) {
            case 'Open':
                return 'o';
            case 'Bug':
                return 'b';
            case 'Not a bug':
                return 'n';
            case 'Duplicate':
                return 'd';
            case 'All':
                return 'a';
            case 'Fixed':
                return 'f';
        }
    }

    appDropSelect();
    function appDropSelect() {
        $("#apps option").each(function () {
            if ($(this).val() === $("#apps").data("selected")) {
                $(this).prop("selected", "true");
            }
        });
    }

    statusDropSelect();
    function statusDropSelect() {
        $("#bug-status option").each(function () {
            if ($(this).val() === $("#bug-status").data("selected")) {
                $(this).prop("selected", "true");
            }
            var old = $(this).val();
            $(this).text(modifyStatusFromBackEnd(old));
        });
    }

    var selectedId = $("#findId").data("id");
    var selectedApp = $("#apps option:selected").val();
    var selectedStatus = modifyStatusToBackEnd($("#bug-status option:selected").val());

    var url = bugsUrl + '/appid/' + selectedApp + '?status=' + selectedStatus + '&id=' + selectedId;

    tableCreation();

    var newUrl = host + '/BugsBunny/index';

    $(function () {
        $("#apps").change(function () {
            selectedApp = $(this).select().val();
            if ($("#findId").val() !== '') {
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus + '&id=' + selectedId;
            } else {
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus;
            }
        });
    });

    $(function () {
        $("#bug-status").change(function () {
            selectedStatus = modifyStatusToBackEnd($(this).select().val());
            if ($("#findId").val() !== '') {
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus + '&id=' + selectedId;
            } else {
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus;
            }
        });
    });

    $(function () {
        $("#findButton").click(function () {
            location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus + '&id=' + $("#findId").val();

        });
    });

    function setBugStatus(id) {
        $("#save-order-" + id).click(function () {
            var editID = $(this).data('item-id');
            var myData;
            var enable = true;

            if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                if (Number($("#order-title-" + id + " option:selected").data("item-id")) !== id) {
                    myData = JSON.stringify({
                        status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                        //mainBugId: Number($("#order-title-" + id + " option:selected").data("item-id"))
                        mainBugId: Number($("#order-title-" + id).data("item-id"))
                    });
                } else {
                    enable = false;
                }
            } else if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                var bugTaskUrl = $("#bug-task-" + id).val();

                if ((bugTaskUrl !== '') && (bugTaskUrl.match(urlRegExp))) {
                    myData = JSON.stringify({
                        status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                        taskUri: $("#bug-task-" + id).val()
                    });
                } else {
                    enable = false;
                    $("#div-for-input-" + id).prop("class", $("#div-for-input-" + id).attr("class") + " has-error");
                }
            } else {
                myData = JSON.stringify({
                    status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val())
                });
            }
            if (enable) {
                updateBugByID(editID, myData);
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus;
            }

        });
    }

    function updateBugByID(editID, myData) {
        $.ajax({
            type: "PUT",
            url: bugsUrl + "/id/" + editID,
            dataType: 'json',
            contentType: "application/json",
            data: myData,
            success: function () {
            }
        });
    }

    function setDuplicate(id) {

        $("#order-title-" + id).on({
            keyup: function (e) {
//                $.getJSON(titlesUrl, function(response){
//                    if (response.code !== 'OK') {
//                        showError(response.errorMessage);
//                    }
//                });

                $("#results-" + id).css('display', 'none');
                $("#results-" + id).empty();
                var search = $(this).val();
                
//                var searchRegExp = encodeURIComponent(search);
//                console.log(searchRegExp);
//                searchRegExp = new RegExp(searchRegExp.replace(/[^0-9a-zD0%B0-D1%8F_]/i), 'i');
                var titlesUrl = apiUrl + '/titles?appId=' + selectedApp + '&search=' + encodeURIComponent(search);

                if (search.length !== 0) {
                    $.getJSON(titlesUrl, function (response) {
                        if (response.code !== 'OK') {
                            showError(response.errorMessage);
                        }
                        $.each(response.data, function (titleId, title) {
                            //if (title.match(searchRegExp)) {
                                $('#results-' + id).append($('<li id="results-' + id + '-' + titleId + '" data-item-id="' + titleId + '"><span class="suggest-name">' + title + '</span></li>'));
                            //}
                        });
                        $('.searchable ul').css('border-bottom-left-radius', '5px');
                        $('.searchable ul').css('border-bottom-right-radius', '5px');
                        $('.searchable li').css('border-top', '1px solid #ccc');
                        $('.searchable li').mouseover(function () {
                            $(this).css('cursor', 'pointer');
                            $(this).css('background-color', '#cbe6f3');
                            $(this).addClass('active');
                        }).mouseout(function () {
                            $(this).css('background-color', 'white');
                            $(this).removeClass('active');
                        });
//                        console.log($('#order-title-' + id).val());
                        $('.searchable li').click(function () {
                            var liId = '#' + $(this).attr('id');
                            $('#order-title-' + id).val($(liId).text());
                            var itemId = $(liId).data('item-id');
                            $('#order-title-' + id).data('item-id', itemId);
                            $('.searchable ul').hide();
                        });
                        $("#results-" + id).show();
                    });
                }
            }
        });


//        $.getJSON(titlesUrl, function (response) {
//            if (response.code !== 'OK') {
//                showError(response.errorMessage);
//            }
//            $.each(response.data, function (titleId, title) {
//                if (Number(titleId) !== id) {
//                    $("#order-title-" + id).append(
//                            '<option data-item-id="' + Number(titleId) + '">' + title + '</option>'
//                            );
//                }
//            });
//        });
    }

    function show(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                        $("#order-title-" + id).css("visibility", "visible");
                    } else {
                        $("#order-title-" + id).css("visibility", "hidden");
                    }
                });
    }

    function setInputReadonly(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                        $("#bug-task-" + id).prop('readonly', false);
                    } else {
                        $("#bug-task-" + id).prop('readonly', true);
                    }
                });
    }

    function copyToPastebin(id, stackTrace) {
        $("#a-copy-" + id).click(
                function () {
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/paste",
                        dataType: 'text',
                        contentType: "text/plain",
                        cache: false,
                        data: stackTrace,
                        success: function (response) {
                            window.open(response);
                        }
                    });
                });
    }

    function collapsible(id) {
        $(".collapse-" + id).on("mouseover", function () {
            $(this).css("cursor", "pointer");
        });
        $(".collapse-" + id).click(
                function () {
                    $(".collapsible-row-" + id).toggle();
                    if ($(".collapsible-title-row-" + id).css("display") !== 'none') {
                        $(".collapsible-title-row-" + id).toggle();
                    }
                });
        $(".collapse-title-" + id).click(
                function () {
                    $(".collapsible-title-row-" + id).toggle();
                });
    }

    function showError(errorMessage) {
        $(".error-message").css("display", "block");
        $(".error-message").append(errorMessage);
    }

    function enableLink(id) {
        $("#bug-a-" + id).click(
                function () {
                    var bugTaskUrl = $("#bug-task-" + id).val();
                    if ((bugTaskUrl !== '') && (bugTaskUrl.match(urlRegExp))) {
                        window.open(bugTaskUrl);
                    }
                });
    }

    function massSetStatus() {
        $(".mass-checkbox").click(
                function () {
                    if ($("#mass-check").hasClass('glyphicon-unchecked')) {
                        $("#mass-check").removeClass('glyphicon-unchecked');
                        $("#mass-check").addClass('glyphicon-check');

                        $(".checks").prop('checked', true);
                    } else if ($("#mass-check").hasClass('glyphicon-check') || $("#mass-check").hasClass('glyphicon-edit')) {
                        $("#mass-check").removeClass('glyphicon-check');
                        $("#mass-check").addClass('glyphicon-unchecked');

                        $(".checks").prop('checked', false);
                    }
                });

//        $("#check-group").change(
//                function () {
//                    var status = modifyStatusToBackEnd($("#check-group option:selected").val());
//                    $("#mass-check").removeClass('glyphicon-unchecked');
//                    $("#mass-check").removeClass('glyphicon-check');
//                    $("#mass-check").addClass('glyphicon-edit');
//                    $(".checks").prop('checked', false);
//                    $(".checks-" + status).prop('checked', true);
//
//
//                });

        $("#save-group").click(function () {
            $(".checks:checked").each(function () {
                var editID = $(this).data('item-id');
                var status = modifyStatusToBackEnd($("#change-group option:selected").val());
                var myData = JSON.stringify({
                    status: status
                });
                updateBugByID(editID, myData);
                location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus;
            });
        });
    }

    function setNewTitle(id) {

        $("#save-title-" + id).click(function () {
            var title = $("#title-change-" + id).val();
            var myData = JSON.stringify({
                title: title
            });
            updateBugByID(id, myData);
            location.href = newUrl + '?appId=' + selectedApp + '&status=' + selectedStatus;
        });
    }

    function tableCreation() {
        $.getJSON(url, function (response) {
            if (response.code !== 'OK') {
                showError(response.errorMessage);
            }
            var count = response.data.length;
            $.each(response.data, function (index, bug) {

                var tmpCrDate = new Date(bug.creationDate);
                var creationDate = tmpCrDate.toISOString().substr(0, 10) + " " + tmpCrDate.toISOString().substr(11, 8);
                var tmpLSDate = new Date(bug.lastSeen);
                var lastSeen = tmpLSDate.toISOString().substr(0, 10) + " " + tmpLSDate.toISOString().substr(11, 8);
                var taskUrl = '';
                if (bug.taskUri !== null) {
                    taskUrl = bug.taskUri;
                }
                $("#bugs-body").append(
                        '<tr class="text-center">\n\
                            <td><input type="checkbox" id="checked-' + bug.id + '" data-item-id="' + bug.id + '" class="checks checks-' + bug.status + '"/></td>\n\
                            <td class="collapse-' + bug.id + '">' + bug.id + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + bug.appId + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + creationDate + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + modifyStatusFromBackEnd(bug.status) + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + bug.lastCount + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + lastSeen + '</td>\n\
                            <td><a id="a-copy-' + bug.id + '">Copy error to haste</a></td>\n\
                        </tr>\n\
                        <tr class="collapsible-row-' + bug.id + '">\n\
                            <td colspan="8">\n\
                                <form class="form-horizontal" role="decorate-form" accept-charset="utf-8">\n\
                                    <div class="form-group lower">\n\
                                        <div class="col-sm-5 col-xs-5">\n\
                                            <div id="div-for-input-' + bug.id + '" class="input-group">\n\
                                                <input type="text" placeholder="Bug Task Url" class="form-control" id="bug-task-' + bug.id + '" readonly value="' + taskUrl + '">\n\
                                                <a id="bug-a-' + bug.id + '" class="btn btn-default input-group-addon" aria-label="Go to bug task" data-toggle="tooltip" title="Go to task">\n\
                                                    <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>\n\
                                                </a>\n\
                                            </div>\n\
                                        </div>\n\
                                        <div class="col-sm-2 col-xs-2">\n\
                                            <select class="form-control select-status" id="order-status-' + bug.id + '">\n\
                                                <option>Open</option>\n\
                                                <option>Bug</option>\n\
                                                <option>Not a bug</option>\n\
                                                <option>Duplicate</option>\n\
                                                <option>Fixed</option>\n\
                                            </select>\n\
                                        </div>\n\
                                        <div class="col-sm-3 col-xs-3 lower">\n\
                                            <div class="searchable">' +
//                                            <select class="form-control select-title" id="order-title-' + bug.id + '">\n\
//                                                <option data-item-id="' + bug.id + '" selected disabled>' + bug.title + '</option>\n\
//                                            </select>\n\
                                                '<input type="search" class="form-control select-title" id="order-title-' + bug.id + '" data-item-id="0" autocomplete="on"/>\n\
                                                <ul id="results-' + bug.id + '"></ul>\n\
                                            </div>\n\
                                        </div>\n\
                                        <button type="button" data-item-id="' + bug.id + '" id="save-order-' + bug.id + '" class="col-sm-1 col-xs-1 btn btn-primary save-order">Save</button>\n\
                                        <button type="button" data-item-id="' + bug.id + '" class="btn btn-primary collapse-title-' + bug.id + '" data-toggle="tooltip" title="Change Title" style="margin-left:10px">\n\
                                            <span class="glyphicon glyphicon-text-size"></span>\n\
                                        </button>\n\
                                    </div>\n\
                                </form>\n\
                            </td>\n\
                        </tr>\n\
                        <tr class="collapsible-title-row-' + bug.id + '">\n\
                            <td colspan="8">\n\
                                <form class="form-horizontal">\n\
                                    <div class="col-sm-10 col-xs-10 lower">\n\
                                        <input type="text" class="form-control" id="title-change-' + bug.id + '" value="' + bug.title + '"/>\n\
                                    </div>\n\
                                    <button type="button" id="save-title-' + bug.id + '" class="btn btn-primary col-sm-offset-1 col-sm-1">Save</button>\n\
                                </form>\n\
                            </td>\n\
                        </tr>\n\
                        <tr class="collapsible-row-' + bug.id + '">\n\
                            <td colspan="8">\n\
                                <p class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10"><strong>' + bug.title + '</strong></p>\n\
                                <p class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10"><strong>RayID: ' + bug.rayID + '</strong></p>\n\
                                <p class="col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10" id="stackTrace-' + bug.id + '">' + bug.stackTrace.split("\n").join("<br>").split(" ").join("&nbsp;") + '</p>\n\
                            </td>\n\
                        </tr>'
                        );

                //$("td").css("overflow", "hidden");
                $("td").css("word-wrap", "break-word");
                $(".select-title").css("visibility", "hidden");
                $(".collapsible-row-" + bug.id).css("display", "none");
                $(".collapsible-row-" + bug.id + " p").css("position", "static");
                $(".collapsible-title-row-" + bug.id).css("display", "none");
                $(".collapse-" + bug.id).css("cursor", "pointer");
                $(".table").css("margin-bottom", 0);
                $(".form-group").css("margin-bottom", 0);
                $(".lower").css('position', 'relative').css('z-index', '0');
                $(".searchable").css('position', 'absolute').css('z-index', '50');
                $('.searchable').css('display', 'inline-block');
                $('.searchable input').css('width', 'inherit');
                $('.searchable ul').css('display', 'none');
                $('.searchable ul').css('width', 'inherit');
                $('.searchable ul').css('list-style', 'none');
                $('.searchable ul').css('margin', '0');
                $('.searchable ul').css('background-color', 'white');
                $('.searchable ul').css('padding', '0');
                $('.searchable ul').css('border', '1px solid #ccc');
                $('.searchable ul').css('margin-top', '-1px');
                $('.searchable ul').css('z-index', '51');
                


                collapsible(bug.id);
                setDuplicate(bug.id);
                show(bug.id);
                setBugStatus(bug.id);
                setInputReadonly(bug.id);
                copyToPastebin(bug.id, bug.stackTrace);
                enableLink(bug.id);
                setNewTitle(bug.id);

                if (count === 1) {
                    $(".collapsible-row-" + bug.id).toggle();
                }
            });
            massSetStatus();
        });
    }

    $(".table").css("margin-bottom", 0);
    $(".form-group").css("margin-bottom", 0);
});