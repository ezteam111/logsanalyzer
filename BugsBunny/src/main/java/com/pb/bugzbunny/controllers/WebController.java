/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.controllers;

import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.services.BugService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Controller
public class WebController {

    @Autowired
    private BugService service;

    @RequestMapping(value = {"/", "/index"})
    public ModelAndView indexNew(
            @RequestParam(value = "appId", required = false, defaultValue = "All") String appId,
            @RequestParam(value = "status", required = false, defaultValue = "o") String status,
            @RequestParam(value = "id", required = false, defaultValue = "0") long id
    ) {

        ModelAndView model = new ModelAndView("html/index");

        List<String> apps = new ArrayList<>();
        apps.add("All");
        apps.addAll(service.getApps().getData());

        List<String> statuses = new ArrayList<>();
        statuses.add("a");
        statuses.addAll(service.getStatuses().getData());

        model.addObject("id", id);
        model.addObject("appId", appId);
        model.addObject("status", status);
        model.addObject("apps", apps);
        model.addObject("statuses", statuses);
        return model;
    }

    @RequestMapping(value = "/angular")
    public ModelAndView angular(
            @RequestParam(value = "appId", required = false, defaultValue = "All") String appId,
            @RequestParam(value = "status", required = false, defaultValue = "o") String status,
            @RequestParam(value = "id", required = false, defaultValue = "0") long id
    ) {

        ModelAndView model = new ModelAndView("html/index_angular");

        List<String> apps = new ArrayList<>();
        apps.add("All");
        apps.addAll(service.getApps().getData());

        List<String> statuses = new ArrayList<>();
        statuses.add("a");
        statuses.addAll(service.getStatuses().getData());

        model.addObject("id", id);
        model.addObject("appId", appId);
        model.addObject("status", status);
        model.addObject("apps", apps);
        model.addObject("statuses", statuses);
        return model;
    }

    @RequestMapping(value = "/panels")
    public String table() {
        return "html/index_panels";
    }

    @RequestMapping(value = "/table2")
    public String table2() {
        return "html/table2";
    }

    @RequestMapping(value = "/goto")
    public ModelAndView bug(@RequestParam("id") long id) {
        Bug bug = service.getBugById(id);
        ModelAndView mav = new ModelAndView("html/bug");
        mav.addObject("bug", bug);
        return mav;
    }

}
