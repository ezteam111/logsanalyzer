/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.controllers;

import com.pb.bugzbunny.beans.Analyzer;
import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.MyResponse;
import com.pb.bugzbunny.services.BugService;
import com.pb.bugzbunny.services.MyHttpClient;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@RestController
@RequestMapping("/api/")

public class BugController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BugService bugService;
//    @Autowired
//    private BugsDaoHiber hiberDao;
    @Autowired
    private Analyzer analyser;
    @Autowired
    private MyHttpClient client;

    @RequestMapping(value = "/bugs/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bug> getBugList() {
        return bugService.getAllBugs();
    }

//    @RequestMapping(value = "/hiber/bugs/list", produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<BugEntity> getHiberBugList() {
//        return hiberDao.getAllBugs();
//    }
    @RequestMapping(value = "/bugs/id/{id}")
    public Bug getBug(@PathVariable("id") long id) {
        return bugService.getBugById(id);
    }

    @RequestMapping(value = "/bugs/id/{id}", method = RequestMethod.PUT)
    public void updateBugByID(@PathVariable("id") long id, @RequestBody Bug bug) {
//        logger.info("ID : " + id + "\t" + bug.toString());
        bugService.updateBugByID(id, bug);

    }

    @RequestMapping(value = "/bugs/id", method = RequestMethod.PUT)
    public void updateMainBugId(@RequestParam("id") long id, @RequestParam("mainBugId") long mainBugId) {
        if (bugService.getBugById(id) != null && bugService.getBugById(mainBugId) != null) {
            bugService.updateMainBugId(id, mainBugId);
        }
    }

    @RequestMapping(value = "/bugs/appid/{appId:.+}")
    public MyResponse<List<Bug>> getBugByAppIdAndStatus(@PathVariable("appId") String appId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "id", required = false, defaultValue = "0") long id) {

        return bugService.getBugByAppIdAndStatus(appId, status, id);
    }

    @RequestMapping(value = "/apps")
    public MyResponse<List<String>> getApps() {
        return bugService.getApps();
    }

    @RequestMapping(value = "/statuses")
    public MyResponse<List<String>> getStatuses() {
        return bugService.getStatuses();
    }

    @RequestMapping(value = "/titles")
    public MyResponse<Map<Long, String>> getIdsAndTitles(@RequestParam("appId") String appId, @RequestParam("search") String search) {
        return bugService.getIdsAndTitles(appId, search);
    }

    @RequestMapping(value = "/paste", method = RequestMethod.POST)
    public String toHaste(@RequestBody String stackTrace) {
        return client.copyToHaste(stackTrace);
    }

    /**
     * Check new bugs in elastic
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/fetch-now")
    public String onFetchNow() throws Exception {
        analyser.analyze();
        return "ok";
    }
    
    /**
     * Send data to corezoid
     * @return 
     */
    @RequestMapping(value = "/send-now")
    public String sendNow() {
        analyser.sendMail();
        return "ok";
    }
}
