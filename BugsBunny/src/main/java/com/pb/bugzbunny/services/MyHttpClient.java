/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.services;

import com.pb.bugzbunny.dao.elastic.ElasticClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */

@Service
public class MyHttpClient {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MyHttpClient.class);
    
    public String copyToHaste(String body) {
        String result = "";
        String domain = "https://haste.pb.ua";
        HttpClient client = HttpClients.createDefault();
        try {
            HttpPost post = new HttpPost(domain + "/documents");
            post.setEntity(new StringEntity(body));

            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStreamReader isp = new InputStreamReader(response.getEntity().getContent());
                BufferedReader rd = new BufferedReader(isp);

                String line = rd.readLine();
                if (line != null) {
                    JSONParser parser = new JSONParser();
                    JSONObject jsonObj = (JSONObject) parser.parse(line);
                    result = (String) jsonObj.get("key");
                }
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(MyHttpClient.class.getName()).log(Level.SEVERE, null, ex);
            logger.error("ERROR", ex);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(MyHttpClient.class.getName()).log(Level.SEVERE, null, ex);
            logger.error("ERROR", ex);
        }
        return domain + "/" + result;
    }
}
