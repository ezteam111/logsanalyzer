/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.services;

import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.ScanResult;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public interface BugSourceService {

    public ScanResult<Bug> findBugs(String from, String to, int firstResult, long maxResults) throws Exception;
}
