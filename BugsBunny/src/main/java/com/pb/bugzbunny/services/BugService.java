/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.services;

import com.pb.bugzbunny.dao.jdbc.BugsDao;
import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.MyResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Service
public class BugService {

    org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BugsDao dao;

    public MyResponse<List<String>> getStatuses() {
        MyResponse<List<String>> response = new MyResponse(null, null, HttpStatus.OK);

        try {
            response.setData(dao.getStatuses());
        } catch (Exception ex) {
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
            response.setErrorMessage(ex.toString());
            logger.error("ERROR",ex);
        }
        return response;
    }

    public MyResponse<List<String>> getApps() {
        MyResponse<List<String>> response = new MyResponse(null, null, HttpStatus.OK);

        try {
            response.setData(dao.getApps());
        } catch (Exception ex) {
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
            response.setErrorMessage(ex.toString());
            logger.error("ERROR",ex);
        }
        return response;
    }

    public MyResponse<List<Bug>> getBugByAppIdAndStatus(String appId, String status, long id) {
        MyResponse<List<Bug>> response = new MyResponse(null, null, HttpStatus.OK);

        try {
            if (id == 0) {
                if (status != null) {
                    if ("a".equals(status) && "All".equals(appId)) {
                        response.setData(dao.getAllBugs());
                    } else if ("a".equals(status)) {
                        response.setData(dao.getBugsByAppId(appId));
                    } else if ("All".equals(appId)) {
                        response.setData(dao.getBugsByStatus(status));
                    } else {
                        response.setData(dao.getBugsByAppIdAndStatus(appId, status));
                    }
                } else {
                    response.setData(dao.getBugsByAppId(appId));
                }
            } else {
                List<Bug> list = new ArrayList<>();
                list.add(dao.getBugById(id));
                response.setData(list);
            }
        } catch (Exception ex) {
            response.setErrorMessage(ex.toString());
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
            logger.error("ERROR",ex);
        }

        return response;
    }

    public MyResponse<Map<Long, String>> getIdsAndTitles(String appId, String search) {
        MyResponse<Map<Long, String>> response = new MyResponse(null, null, HttpStatus.OK);
        
        try {
            response.setData(dao.getIDsAndTitlesForCurrentGroup(appId, search));
        } catch (Exception ex) {
            response.setErrorMessage(ex.toString());
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
            logger.error("ERROR",ex);
        }

        return response;
    }

    public void updateLastSeenAndCount(Bug bug) {
        dao.updateLastSeenAndCount(bug);
        if ("f".equals(dao.getStatusByHash(bug.getHashCheckSum()))) {
            dao.updateBugStatusByHash(bug.getHashCheckSum(), "o");
        }
    }
    
    public long findExistingBugId(String appId, String hashCheckSum) {
        return dao.findExistingBugId(appId, hashCheckSum);
    }
    
    public void insertNewBug(Bug bug){
        dao.insertNewBug(bug);
    }
    
    public Date getLastSeen(String appId, String hashCheckSum){
        return dao.getLastSeen(appId, hashCheckSum);
    }
    
    public int getLastCount(String appId, String hashCheckSum) {
        return dao.getLastCount(appId, hashCheckSum);
    }
    
    public long getExistingMainBugId(String appId, String hashCheckSum) {
        return dao.getExistingMainBugId(appId, hashCheckSum);
    }
    
    public Bug getBugById(long id) {
        return dao.getBugById(id);
    }
    
    public List<Bug> getOpenBugs() {
        return dao.getOpenBugs();
    }
    
    public void updateBugByID(long id, Bug bug){
        if (dao.getBugById(id) != null) {
            if (bug.getStatus() != null) {
                dao.updateBugStatus(id, bug);
            } else if (bug.getTitle() != null) {
                dao.updateTitle(id, bug.getTitle());
            }
        }
    }
    
    public void updateMainBugId(long id, long mainBugId){
        dao.updateMainBugId(id, mainBugId);
    }
    
    public List<Bug> getAllBugs() {
        return dao.getAllBugs();
    }
}
