/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.services;

import com.pb.bugzbunny.dao.elastic.ElasticClient;
import com.pb.bugzbunny.dto.elastic.ElasticSearchResult;
import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.ScanResult;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Service
public class ElasticErrorSourceService implements BugSourceService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${bugzbunny.elastic.url}")
    String url;

    @Value("${bugzbunny.client.queryErrorCode}")
    String queryErrorCode;
    @Value("${bugzbunny.client.queryErrorField}")
    String queryErrorField;
    @Value("${bugzbunny.client.queryErrorDateField}")
    String queryErrorDateField;
    @Value("${bugzbunny.client.queryAppIdField}")
    String queryAppIdField;
    @Value("${bugzbunny.client.queryStackTraceField}")
    String queryStackTraceField;
    @Value("${bugzbunny.client.queryRayIDField}")
    String queryRayIDField;

    @Override
    public ScanResult<Bug> findBugs(String from, String to, int firstResult, long maxResults) throws Exception {

        List<Bug> bugList;
        bugList = new ArrayList<>();

        ElasticClient client = new ElasticClient();

        ElasticSearchResult searchResult = client.search(queryErrorField, queryErrorCode, queryErrorDateField, from, to, firstResult, maxResults, url);

        for (JSONObject result : searchResult.getResultList()) {
            Bug bug = new Bug();
            bug.setAppId((String) result.get(queryAppIdField));
            bug.setStackTrace((String) result.get(queryStackTraceField));
            bug.setRayID(getElem(result, queryRayIDField));
            String strDate = (String) result.get(queryErrorDateField);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse(strDate.substring(0, 10) + " " + strDate.substring(11));
            bug.setLastSeen(date);
            bugList.add(bug);
        }

        return new ScanResult<>(searchResult.getTotal(), bugList);
    }

    /*
    Надо подумать над оптимизацией этого куска
    */
    public String getElem(JSONObject jsonObject, String row) {
        String[] fields = row.split("\\.");
        String result = null;
        try {
            for (int i = 0; i < fields.length; i++) {
                JSONObject tmp = jsonObject;
                if (i == fields.length - 1) {
                    result = (String) tmp.get(fields[i]);
                } else {
                    jsonObject = (JSONObject) tmp.get(fields[i]);
                }
            }
        } catch (NullPointerException ex) {
            logger.warn("WARNING", ex);
        }
        return result;
    }
}
