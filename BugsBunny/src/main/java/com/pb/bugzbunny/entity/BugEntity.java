/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Entity
@Table(name = "authorizedBugsHiber", schema = "bugsbunny", uniqueConstraints = 
        @UniqueConstraint(columnNames = {"appId", "hashCheckSum"}))
//@Table(name = "authorizedBugs", schema = "bugsbunny")
public class BugEntity implements Serializable {
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name = "id", unique = true, nullable = false)
    private int id;
    
    @Column(name = "appId", nullable = false)
    private String appId;
    
    @Column(name = "stackTrace", nullable = false)
    private String stackTrace;
    
    @Column(name = "hashCheckSum", nullable = false)
    private String hashCheckSum;
    
    @Column(name = "status", nullable = false, length = 1)
    @ColumnDefault(value = "o")
    private String status;
    
    @Column(name = "lastCount", nullable = false)
    @ColumnDefault(value = "1")
    private int lastCount;
    
    @Column(name = "creationDate", nullable = false)
    @ColumnDefault(value = "now()")
    @Temporal(TemporalType.DATE)
    private Date creationDate;
    
    
    @Column(name = "lastSeen", nullable = false)
    @ColumnDefault(value = "now()")
    @Temporal(TemporalType.DATE)
    private Date lastSeen;
    
    @Column(name = "mainBugId", nullable = false)
    @ColumnDefault(value = "0")
    private int mainBugId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getHashCheckSum() {
        return hashCheckSum;
    }

    public void setHashCheckSum(String hashCheckSum) {
        this.hashCheckSum = hashCheckSum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getLastCount() {
        return lastCount;
    }

    public void setLastCount(int lastCount) {
        this.lastCount = lastCount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public int getMainBugId() {
        return mainBugId;
    }

    public void setMainBugId(int mainBugId) {
        this.mainBugId = mainBugId;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + "id=" + id + ", appId=" + appId
                + ", stackTrace=" + stackTrace + ", hashCheckSum=" + hashCheckSum
                + ", status=" + status + ", lastCount=" + lastCount + ", creationDate=" + creationDate
                + ", lastSeen=" + lastSeen + ", mainBugId=" + mainBugId + ']';
    }
    
    
}
