/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.beans;

import com.pb.bugzbunny.dao.jdbc.BugsDao;
import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.ScanResult;
import com.pb.bugzbunny.services.BugService;
import com.pb.bugzbunny.services.ElasticErrorSourceService;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Component
public class Analyzer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${bugzbunny.corezoid.url}")
    private String corezoidUrl;

    @Autowired
    private BugService bugService;

    @Autowired
    private ElasticErrorSourceService elasticService;

    @Scheduled(cron = "${bugzbunny.scheduler.analyzer}")
    public void analyze() throws Exception {

        long total;
        int startFrom = 0;
        int blockSize = 3;
        
        Calendar calendar = Calendar.getInstance();
        String todayStartOfDay = calendar.getTime().toInstant().toString().substring(0, 11) + "00:00:00";
        calendar.add(Calendar.DATE, -1);
        String yesterdayStartOfDay = calendar.getTime().toInstant().toString().substring(0, 11) + "00:00:00";
        
        do {
            ScanResult<Bug> searchResult = elasticService.findBugs(yesterdayStartOfDay, todayStartOfDay, startFrom, blockSize);
            total = searchResult.getHits();

            checkBug(searchResult.getBugs());

            startFrom += blockSize;
        } while (startFrom < total);
    }

    /**
     * Сверяет новые баги с существующими в БД.
     * @param bugList
     * @throws NoSuchAlgorithmException 
     */
    private void checkBug(List<Bug> bugList) throws NoSuchAlgorithmException {

        for (Bug obj : bugList) {
            Bug bug = new Bug();
            bug.setAppId(obj.getAppId());
            bug.setStackTrace((String) obj.getStackTrace());
            bug.setHashCheckSum(createHash(bug.getStackTrace()));
            bug.setTitle(bug.getStackTrace().split("\n")[0].trim());
            bug.setRayID(obj.getRayID());
            
            if (bugService.findExistingBugId(bug.getAppId(), bug.getHashCheckSum()) == -1) {
                bug.setLastSeen(obj.getLastSeen());
                bug.setCreationDate(obj.getLastSeen());
                bugService.insertNewBug(bug);
            } else {
                bug.setLastSeen(bugService.getLastSeen(bug.getAppId(), bug.getHashCheckSum()));
                bug.setLastCount(bugService.getLastCount(bug.getAppId(), bug.getHashCheckSum()));

                long mainBugId = bugService.getExistingMainBugId(bug.getAppId(), bug.getHashCheckSum());
                if (mainBugId != 0) {
                    bug = bugService.getBugById(mainBugId);
                }
                if (checkLastSeenForCounter(bug.getLastSeen(), obj.getLastSeen())) {
                    bug.setLastCount(bug.getLastCount() + 1);
                } else {
                    bug.setLastCount(1);
                }

                boolean check = bug.getLastSeen().after(obj.getLastSeen());
                if(!check){
                    bug.setLastSeen(obj.getLastSeen());
                }
                
                bugService.updateLastSeenAndCount(bug);
            }
        }
    }

    /**
     * Отправляет новые баги в корезоид
     */
    @Scheduled(cron = "${bugzbunny.scheduler.sender}")
    public void sendMail() {

        List<Bug> newBugs;
        newBugs = bugService.getOpenBugs();

        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> map = new HashMap<>();

        if (!newBugs.isEmpty()) {
            for (Bug newBug : newBugs) {
                restTemplate.postForObject(corezoidUrl, newBug, Bug.class);
            }
        }
    }
    
    /**
     * На основе полученной строки создает md5 шифр.
     * @param str
     * @return String MD5
     * @throws NoSuchAlgorithmException 
     */
    private String createHash(String str) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte data[] = str.substring(str.indexOf("\n")).getBytes();
        md.update(data, 0, data.length);
        BigInteger i = new BigInteger(1, md.digest());
        return String.format("%1$032X", i);
    }

    /**
     * Сравнивает даты для счетчика. 
     * @param current Текущая дата
     * @param lastSeen Дата, когда баг наблюдался последний раз
     * @return false если даты не совпадают, true если совпадают
     */
    private boolean checkLastSeenForCounter(Date current, Date lastSeen) {
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(current);
        Calendar lastSeenCalendar = Calendar.getInstance();
        lastSeenCalendar.setTime(lastSeen);
        return currentDate.getTime().toInstant().toString().substring(0, 10).equals(lastSeenCalendar.getTime().toInstant().toString().substring(0, 10));
    }
}
