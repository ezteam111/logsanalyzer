/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dao.elastic;

import com.pb.bugzbunny.dto.elastic.ElasticSearchResult;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ElasticClient {
    
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ElasticClient.class);

    public ElasticSearchResult search(String queryErrorField, String queryErrorCode, String queryErrorDateField, String yesterdayStartOfDay, String todayStartOfDay, int startFrom, long blockSize, String urlString) throws ParseException, Exception {

        List<JSONObject> resultList = new ArrayList<>();
        
        URL url = new URL(urlString + "/_search");
        logger.info("URL : {}", url.toString());
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
//***********************************************************************************
        JSONObject errorDateFieldJSON = new JSONObject();
        errorDateFieldJSON.put("from", yesterdayStartOfDay);
        errorDateFieldJSON.put("to", todayStartOfDay);
        
        JSONObject errorDate = new JSONObject();
        errorDate.put(queryErrorDateField, errorDateFieldJSON);
        
        JSONObject range = new JSONObject();
        range.put("range", errorDate);
        
        JSONObject query = new JSONObject();
        query.put("query", queryErrorField+" : (\"" + queryErrorCode + "\")");
        
        JSONObject query_string = new JSONObject();
        query_string.put("query_string", query);
        
        JSONArray must = new JSONArray();
        must.add(query_string);
        must.add(range);
        
        JSONObject bool = new JSONObject();
        bool.put("must", must);
        
        JSONObject top_query = new JSONObject();
        top_query.put("bool", bool);
        
        JSONObject request = new JSONObject();
        request.put("from", startFrom);
        request.put("size", blockSize);
        request.put("query", top_query);
//***********************************************************************************
        
//        String req = "{\"from\" : " + startFrom + ", \"size\" : " + blockSize + ",\n"
//                + "  \"query\": {\n"
//                + "    \"bool\": {\n"
//                + "      \"must\": [\n"
//                + "        {\n"
//        	+ "		\"query_string\":{\n"
//        	+ "			\"query\":\""+queryErrorField+":(\\\""+queryErrorCode+"\\\")\""
//        	+ "		}\n"
//                + "        },\n"
//                + "        {\n"
//                + "          \"range\": {\n"
//                + "            \"" + queryErrorDateField + "\": {\n"
//                + "              \"from\": \"" + yesterdayStartOfDay + "\",\n"
//                + "              \"to\": \"" + todayStartOfDay + "\"\n"
//                + "            }\n"
//                + "          }\n"
//                + "        }\n"
//                + "      ]\n"
//                + "    }\n"
//                + "  }\n"
//                + "}";

        writer.write(request.toString());
        logger.info("Request Body : {}", request.toJSONString());
        writer.flush();

        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = (JSONObject) parser.parse(reader);
        logger.info("Response Body : {}", jsonObj.toJSONString());
        JSONObject hitsObject = (JSONObject) jsonObj.get("hits");
        long total = (long) hitsObject.get("total");
        JSONArray hitsArr = (JSONArray) hitsObject.get("hits");
        
        JSONObject hit;
        JSONObject source;

        for (Object hitsArr1 : hitsArr) {
            hit = (JSONObject) hitsArr1;
            source = (JSONObject) hit.get("_source");
            resultList.add(source);
        }
        reader.close();
        writer.close();
        return new ElasticSearchResult(total, resultList);
    }
}
