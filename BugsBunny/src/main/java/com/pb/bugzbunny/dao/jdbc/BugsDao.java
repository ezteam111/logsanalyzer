/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dao.jdbc;

import com.pb.bugzbunny.dto.Bug;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
@Repository
public class BugsDao {

    private static final String SELECT_ALL_FIELDS
            = "select id, appId, stackTrace, hashCheckSum, lastCount, creationDate, "
            + "mainBugId, status, lastSeen, title, taskUri, rayID from authorizedBugs";

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public long findExistingBugId(String appId, String hashCheckSum) {

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select id from authorizedBugs where appId=? and hashCheckSum=?",
                appId, hashCheckSum);
        long id = -1;
        for (Map row : rows) {
            id = ((Number) (row.get("id"))).longValue();
        }
        return id;
    }

    public void insertNewBug(Bug bug) {
        jdbcTemplate.update("insert into authorizedBugs(appId, stackTrace, hashCheckSum, title, status, creationDate, lastSeen, rayID)"
                + " values(?,?,?,?,'o',?,?,?)",
                bug.getAppId(), bug.getStackTrace(), bug.getHashCheckSum(), bug.getTitle(), bug.getCreationDate(), bug.getLastSeen(), bug.getRayID());
    }

    public List<Bug> getOpenBugs() {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " where status='o'", new BugRowMapper());
    }

    public List<Bug> getAllBugs() {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " order by lastSeen desc", new BugRowMapper());
    }

    public Bug getBugById(long id) {
        return jdbcTemplate.queryForObject(
                SELECT_ALL_FIELDS + " where id=?", new BugRowMapper(),
                id);
    }

    public List<Bug> getBugsByAppId(String appId) throws Exception {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " where appId=? order by lastSeen desc", new BugRowMapper(),
                appId);
    }

    public List<Bug> getBugsByAppIdAndStatus(String appId, String status) throws Exception {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " where appId=? and status=? order by lastSeen desc", new BugRowMapper(),
                appId, status);
    }

    public List<Bug> getBugsByIdAndAppIdAndStatus(long id, String appId, String status) throws Exception {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " where id=? and appId=? and status=?", new BugRowMapper(),
                id, appId, status);
    }

    public List<String> getApps() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select distinct(appId) from authorizedBugs");
        List<String> appIdList = new ArrayList<>();
        rows.forEach((row) -> {
            appIdList.add((String) row.get("appId"));
        });
        return appIdList;
    }

    public List<Bug> getBugsByStatus(String status) throws Exception {
        return jdbcTemplate.query(
                SELECT_ALL_FIELDS + " where status=? order by lastSeen desc", new BugRowMapper(),
                status);
    }

    public List<String> getStatuses() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select distinct(status) from authorizedBugs");
        List<String> statusList = new ArrayList<>();
        rows.forEach((row) -> {
            statusList.add((String) row.get("status"));
        });
        return statusList;
    }

    public Map<Long, String> getIDsAndTitlesForCurrentGroup(String appId, String search) throws Exception {

        List<Map<String, Object>> rows = null;
        Map<Long, String> map = new HashMap<>();

        if (appId.equals("All")) {
            rows = jdbcTemplate.queryForList("select id, appId, title, status from authorizedBugs where status in ('o', 'b') and title like ?", search + "%");
            for (Map row : rows) {
                
                long id = ((Number) (row.get("id"))).longValue();
                String title = (String) row.get("title");
                String responseAppId = (String) row.get("appId");
                String responseStatus = (String) row.get("status");
                map.put(id, responseAppId + "." + refactorStatus(responseStatus) + "." + id + ". " + title);
            }
        } else {
            rows = jdbcTemplate.queryForList("select id, title, status from authorizedBugs where appId=? and status in ('o', 'b') and title like ?", appId, search + "%");
            for (Map row : rows) {
                long id = ((Number) (row.get("id"))).longValue();
                String title = (String) row.get("title");
                String responseStatus = (String) row.get("status");
                map.put(id, refactorStatus(responseStatus) + "." + id + ". " + title);
            }
        }

        return map;
    }

    private String refactorStatus(String in) {
        String out = null;
        switch (in) {
            case "a":
                out = "All";
                break;
            case "b":
                out = "Bug";
                break;
            case "d":
                out = "Duplicate";
                break;
            case "o":
                out = "Open";
                break;
            case "f":
                out = "Fixed";
                break;
        }
        return out;
    }

    public void updateLastSeenAndCount(Bug bug) {
        jdbcTemplate.update("update authorizedBugs set lastSeen=?, lastCount=? where appId=? and hashCheckSum=?",
                bug.getLastSeen(), bug.getLastCount(), bug.getAppId(), bug.getHashCheckSum());
    }

    public Date getLastSeen(String appId, String hashCheckSum) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select lastSeen from authorizedBugs where appId=? and hashCheckSum=?",
                appId, hashCheckSum);
        Date lastSeen = new Date();
        for (Map row : rows) {
            lastSeen = (Date) row.get("lastSeen");
        }
        return lastSeen;
    }

    public int getLastCount(String appId, String hashCheckSum) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select lastCount from authorizedBugs where appId=? and hashCheckSum=?",
                appId, hashCheckSum);
        int lastCount = 0;
        for (Map row : rows) {
            lastCount = (int) row.get("lastCount");
        }
        return lastCount;
    }

    public long getExistingMainBugId(String appId, String hashCheckSum) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select mainBugId from authorizedBugs where appId=? and hashCheckSum=?",
                appId, hashCheckSum);
        long mainBugId = 0;
        for (Map row : rows) {
            mainBugId = ((Number) (row.get("mainBugId"))).longValue();
        }
        return mainBugId;
    }

    public String getStatusByHash(String hash) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                "select status from authorizedBugs where hashCheckSum=?",
                hash);
        String status = null;
        for (Map row : rows) {
            status = (String) row.get("status");
        }
        return status;
    }

    public void updateBugStatusByHash(String hash, String status) {
        jdbcTemplate.update("update authorizedBugs set status=? where hashCheckSum=?",
                status, hash);
    }

    public void updateBugStatus(long id, Bug bug) {
        logger.info(bug.getMainBugId() + " : " + bug.getStatus());
        if (bug.getMainBugId() != 0) {
            jdbcTemplate.update("update authorizedBugs set status=?, mainBugId=? where id=?",
                    bug.getStatus(), bug.getMainBugId(), id);
        } else if (bug.getTaskUri() != null) {
            jdbcTemplate.update("update authorizedBugs set status=?, taskUri=? where id=?",
                    bug.getStatus(), bug.getTaskUri(), id);
        } else {
            jdbcTemplate.update("update authorizedBugs set status=? where id=?",
                    bug.getStatus(), id);
        }
    }

    public void updateMainBugId(long id, long mainBugId) {
        jdbcTemplate.update("update authorizedBugs set mainBugId=? where id=?",
                mainBugId, id);
    }

    public void updateTitle(long id, String title) {
        jdbcTemplate.update("update authorizedBugs set title=? where id=?",
                title, id);
    }
}
