/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dao.jdbc;

import com.pb.bugzbunny.dto.Bug;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;



/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class BugRowMapper implements RowMapper<Bug>{

    @Override
    public Bug mapRow(ResultSet rs, int rowNum) throws SQLException {
        Bug bug = new Bug();
        bug.setId(((Number)rs.getInt("id")).longValue());
        bug.setAppId(rs.getString("appId"));
        bug.setHashCheckSum(rs.getString("hashCheckSum"));
        bug.setStackTrace(rs.getString("stackTrace"));
        bug.setLastCount(rs.getInt("lastCount"));
        bug.setCreationDate(rs.getTimestamp("creationDate"));
        bug.setLastSeen(rs.getTimestamp("lastSeen"));
        bug.setStatus(rs.getString("status"));
        bug.setMainBugId(((Number)rs.getInt("mainBugId")).longValue());
        bug.setTitle(rs.getString("title"));
        bug.setTaskUri(rs.getString("taskUri"));
        bug.setRayID(rs.getString("rayID"));
        return bug;
    }
    
}
