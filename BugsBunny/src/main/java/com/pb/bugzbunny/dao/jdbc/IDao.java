/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dao.jdbc;

import com.pb.bugzbunny.entity.BugEntity;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public interface IDao {

    public List<BugEntity> getOpenBugs();

    public List<BugEntity> getAllBugs();

    public List<BugEntity> getBugsByAppId(String appId);

    public List<BugEntity> getBugsByAppIdAndStatus(String appId, String status);

    public List<BugEntity> getBugsByStatus(String status);

    public List<String> getStatuses();

    public List<String> getApps();

    public BugEntity getBugById(long id);

    public Date getLastSeen(String appId, String hashCheckSum);

    public int findExistingBugId(String appId, String hashCheckSum);

    public int getLastCount(String appId, String hashCheckSum);

    public int getExistingMainBugId(String appId, String hashCheckSum);

    public void insertNewBug(BugEntity bug);

    public void updateLastSeenAndCount(BugEntity bug);

    public void updateBugStatus(long id, BugEntity bug);

    public void updateMainBugId(int id, int mainBugId);
}
