/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dto;

import org.springframework.http.HttpStatus;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class MyResponse<T> {
    private T data;
    private String errorMessage;
    private HttpStatus code;

    public MyResponse() {
    }

    public MyResponse(T data, String errorMessage, HttpStatus code) {
        this.data = data;
        this.errorMessage = errorMessage;
        this.code = code;
    }
    
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }
    
    
}
