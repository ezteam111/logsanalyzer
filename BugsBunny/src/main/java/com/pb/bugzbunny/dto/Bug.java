package com.pb.bugzbunny.dto;

import java.util.Date;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class Bug {

    private long id;
    private String appId;
    private String stackTrace;
    private String hashCheckSum;
    private String status;
    private int lastCount;
    private Date creationDate;
    private Date lastSeen;
    private long mainBugId;
    private String title;
    private String taskUri;
    private String rayID;
    
    public Bug() {
    }

    public String getTaskUri() {
        return taskUri;
    }

    public void setTaskUri(String taskUri) {
        this.taskUri = taskUri;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRayID() {
        return rayID;
    }

    public void setRayID(String rayID) {
        this.rayID = rayID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMainBugId() {
        return mainBugId;
    }

    public void setMainBugId(long mainBugId) {
        this.mainBugId = mainBugId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getHashCheckSum() {
        return hashCheckSum;
    }

    public void setHashCheckSum(String hashCheckSum) {
        this.hashCheckSum = hashCheckSum;
    }

    public int getLastCount() {
        return lastCount;
    }

    public void setLastCount(int lastCount) {
        this.lastCount = lastCount;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[" + "id=" + id + ", appId=" + appId + ", stackTrace=" 
                + stackTrace + ", hashCheckSum=" + hashCheckSum + ", status=" + status + ", lastCount=" 
                + lastCount + ", creationDate=" + creationDate + ", lastSeen=" + lastSeen + ", mainBugId="
                + mainBugId + ", taskUri=" + taskUri + ", rayID=" + rayID + ']';
    }

}
