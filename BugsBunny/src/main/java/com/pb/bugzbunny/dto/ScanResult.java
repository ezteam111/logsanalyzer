/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dto;

import java.util.List;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ScanResult<T> {
    private long hits;
    private List<Bug> bugs;

    public ScanResult(long hits, List<Bug> bugs) {
        this.hits = hits;
        this.bugs = bugs;
    }
    
    public long getHits() {
        return hits;
    }

    public void setHits(long hits) {
        this.hits = hits;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }
    
    
}
