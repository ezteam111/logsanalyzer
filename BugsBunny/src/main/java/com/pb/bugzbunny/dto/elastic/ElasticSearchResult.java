/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.dto.elastic;

import java.util.List;
import org.json.simple.JSONObject;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ElasticSearchResult {
    
    private long total;
    private List<JSONObject> resultList;

    public ElasticSearchResult() {
    }

    public ElasticSearchResult(long total, List<JSONObject> resultList) {
        this.total = total;
        this.resultList = resultList;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<JSONObject> getResultList() {
        return resultList;
    }

    public void setResultList(List<JSONObject> resultList) {
        this.resultList = resultList;
    }
}
