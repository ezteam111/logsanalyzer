/*
 * To change this license title, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    var host = window.location.protocol + '//' + window.location.host;

    var apiUrl = host + '/BugsBunny/api';

    var bugsUrl = apiUrl + '/bugs';
    var appsUrl = apiUrl + '/apps';
    var statusUrl = apiUrl + '/statuses';

    var bool = false;
    
    var myregexp = /^https?:\/\/[a-z0-9./]+/i;

    $.getJSON(appsUrl, function (response) {
        if (response.code !== 'OK') {
            showError(response.errorMessage);
        }
        var appData = response.data;
        if ($.cookie("apps") !== null) {
            for (var i in appData) {
                if ($.cookie("apps") === appData[i]) {
                    bool = true;
                    break;
                }
            }
        }
        if (($.cookie("apps") === null) || (bool === false)) {
            $.cookie("apps", "All", {expires: 1});
        }
    });

    function modifyStatusFromBackEnd(status) {
        switch (status) {
            case 'o':
                return 'Open';
            case 'b':
                return 'Bug';
            case 'n':
                return 'Not a bug';
            case 'd':
                return 'Duplicate';
            case 'a':
                return 'All';
        }
    }

    function modifyStatusToBackEnd(status) {
        switch (status) {
            case 'Open':
                return 'o';
            case 'Bug':
                return 'b';
            case 'Not a bug':
                return 'n';
            case 'Duplicate':
                return 'd';
            case 'All':
                return 'a';
        }
    }

    $.getJSON(statusUrl, function (response) {
        if (response.code !== 'OK') {
            showError(response.errorMessage);
        }
        var statusData = response.data;
        if ($.cookie("bug-status") !== null) {
            for (var i in statusData) {
                if ($.cookie("bug-status") === statusData[i]) {
                    bool = true;
                    break;
                }
            }
        }
        if (($.cookie("bug-status") === null) || (bool === false)) {
            $.cookie("bug-status", "a", {expires: 1});
        }
    });

    var url = bugsUrl + '/appid/' + $.cookie("apps") + '?status=' + $.cookie("bug-status");

    $.getJSON(appsUrl)
            .then(sortAppDropdown)
            .then(sortStatusDropdown)
            .then(panelsCreation);

    function sortAppDropdown(response) {
        if (response.code !== 'OK') {
            showError(response.errorMessage);
        }
        $("#apps").append('<option data-item-id="All">All</option>');
        $.each(response.data, function (index, appId) {
            if (appId !== $.cookie("apps")) {
                $("#apps").append('\n\
                    <option data-item-id="' + appId + '">' + appId + '</option>');
            } else {
                $("#apps").append('\n\
                    <option data-item-id="' + appId + '" selected>' + appId + '</option>');
            }
        });
    }

    function sortStatusDropdown() {
        $.getJSON(statusUrl, function (response) {
            if (response.code !== 'OK') {
                showError(response.errorMessage);
            }
            $("#bug-status").append('<option data-item-id="a">' + modifyStatusFromBackEnd('a') + '</option>');
            $.each(response.data, function (index, status) {
                if (status !== $.cookie("bug-status")) {
                    $("#bug-status").append('\n\
                    <option data-item-id="' + status + '">' + modifyStatusFromBackEnd(status) + '</option>');
                } else {
                    $("#bug-status").append('\n\
                    <option data-item-id="' + status + '" selected>' + modifyStatusFromBackEnd(status) + '</option>');
                }
            });
        });
    }

    $(function () {
        $("#apps").change(function () {
            $.cookie("apps", $(this).select().val(), {expires: 1});
            location.reload();
        });
    });

    $(function () {
        $("#bug-status").change(function () {
            $.cookie("bug-status", modifyStatusToBackEnd($(this).select().val()), {expires: 1});
            location.reload();
        });
    });

    function setBugStatus(id) {

        $("#save-order-" + id).click(function () {

            var editID = $(this).data('item-id');
            var myData;
            var enable = true;
            
            if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                if (Number($("#order-title-" + id + " option:selected").data("item-id")) !== id) {
                    $("#save-order-" + id).prop("type", "submit");
                    myData = JSON.stringify({
                        status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                        mainBugId: Number($("#order-title-" + id + " option:selected").data("item-id"))
                    });
                } else {
                    enable = false;
                    $("#save-order-" + id).prop("type", "button");
                }

            } else if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                var bugTaskUrl=$("#bug-task-" + id).val();
                
                if ((bugTaskUrl !== '') && (bugTaskUrl.match(myregexp))) {
                    $("#save-order-" + id).prop("type", "submit");
                    
                    myData = JSON.stringify({
                        status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val()),
                        taskUri: $("#bug-task-" + id).val()
                    });
                } else {
                    enable = false;
                    $("#save-order-" + id).prop("type", "button");
                    $("#div-for-input-" + id).prop("class", $("#div-for-input-" + id).attr("class") + " has-error");
                }
            } else {
                $("#save-order-" + id).prop("type", "submit");
                myData = JSON.stringify({
                    status: modifyStatusToBackEnd($("#order-status-" + editID + " option:selected").val())
                });
            }
            if (enable) {
                $.ajax({
                    type: "PUT",
                    url: bugsUrl + "/id/" + editID,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    data: myData,
                    success: function () {
                    }
                });
            }
        });
    }

    function setDuplicate(id) {
        var titlesUrl = apiUrl + '/titles?appId=' + $.cookie("apps") + '&status=' + $.cookie("bug-status");
        $.getJSON(titlesUrl, function (response) {
            if (response.code !== 'OK') {
                showError(response.errorMessage);
            }
            $.each(response.data, function (titleId, title) {
                if (Number(titleId) !== id) {
                    $("#order-title-" + id).append(
                            '<option data-item-id="' + Number(titleId) + '">' + title + '</option>'
                            );
                }
            });
        });
    }

    function show(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Duplicate') {
                        $("#order-title-" + id).css("visibility", "visible");
                    } else {
                        $("#order-title-" + id).css("visibility", "hidden");
                    }
                });
    }

    function setInputReadonly(id) {
        $("#order-status-" + id).change(
                function () {
                    if ($("#order-status-" + id + " option:selected").val() === 'Bug') {
                        $("#bug-task-" + id).prop('readonly', false);
                    } else {
                        $("#bug-task-" + id).prop('readonly', true);
                    }
                });
    }

    function copyToPastebin(id, stackTrace) {
        $("#a-copy-" + id).click(
                function () {
//                    var tmp = $("#stackTrace-"+id).text();
//                    console.log("tmp : " + tmp);
//                    var tmp2 = tmp.split("<br>").join("\n").split("&nbsp;").join(" ");
//                    console.log("tmp2 : " + tmp2);
                    $.ajax({
                        type: "POST",
                        url: apiUrl + "/paste",
                        dataType: 'text',
                        contentType: "text/plain; charset=utf-8",
                        cache: false,
                        data: stackTrace,
//                        data: tmp2,
                        success: function (response) {
                            window.open(response);
                        }
                    });
                });
    }

    function collapsible(id) {
        $(".collapse-" + id).on("mouseover", function () {
            $(this).css("cursor", "pointer");
        });
        $(".collapse-" + id).click(
                function () {
                    $(".collapsible-row-" + id).toggle();
                });

    }

    function showError(errorMessage) {
        $(".error-message").css("display", "block");
        $(".error-message").append(errorMessage);
    }

    function enableLink(id) {
        $("#bug-a-" + id).click(
                function () {
                    var bugTaskUrl=$("#bug-task-" + id).val();
                    if ((bugTaskUrl !== '') && (bugTaskUrl.match(myregexp))) {
                        window.open(bugTaskUrl);
                    }
                });
    }

    function panelsCreation() {
        $.getJSON(url, function (response) {
            if (response.code !== 'OK') {
                showError(response.errorMessage);
            }
            $.each(response.data, function (index, bug) {
                var tmpCrDate = new Date(bug.creationDate);
                var creationDate = tmpCrDate.toISOString().substr(0, 10) + " " + tmpCrDate.toISOString().substr(11, 8);
                var tmpLSDate = new Date(bug.lastSeen);
                var lastSeen = tmpLSDate.toISOString().substr(0, 10) + " " + tmpLSDate.toISOString().substr(11, 8);
                var taskUrl = '';
                if (bug.taskUri !== null)
                    taskUrl = bug.taskUri;
                $("#bugs-body").append(
                        '<tr class="text-center">\n\
                            <td class="collapse-' + bug.id + '" style="cursor: pointer;">' + bug.id + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + bug.appId + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + creationDate + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + modifyStatusFromBackEnd(bug.status) + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + bug.lastCount + '</td>\n\
                            <td class="collapse-' + bug.id + '">' + lastSeen + '</td>\n\
                            <td><a id="a-copy-' + bug.id + '">Copy error to haste</a></td>\n\
                        </tr>\n\
                        <tr class="collapsible-row-' + bug.id + '" style="display: none;">\n\
                            <td colspan="7">\n\
                                <form class="form-horizontal" role="decorate-form">\n\
                                    <div class="form-group">\n\
                                        <div class="col-sm-5">\n\
                                            <div id="div-for-input-' + bug.id + '" class="input-group">\n\
                                                <input type="text" placeholder="Bug Task Url" class="form-control" id="bug-task-' + bug.id + '" readonly value="' + taskUrl + '">\n\
                                                <a id="bug-a-' + bug.id + '" class="btn btn-default input-group-addon" aria-label="Go to bug task" data-toggle="tooltip" title="Go to task">\n\
                                                    <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>\n\
                                                </a>\n\
                                            </div>\n\
                                        </div>\n\
                                        <div class="col-sm-2">\n\
                                            <select class="form-control select-status" id="order-status-' + bug.id + '">\n\
                                                <option>Open</option>\n\
                                                <option>Bug</option>\n\
                                                <option>Not a bug</option>\n\
                                                <option>Duplicate</option>\n\
                                            </select>\n\
                                        </div>\n\
                                        <div class="col-sm-3">\n\
                                            <select class="form-control select-title" id="order-title-' + bug.id + '" style="visibility:hidden;">\n\
                                                <option data-item-id="' + bug.id + '" selected disabled>' + bug.title + '</option>\n\
                                            </select>\n\
                                        </div>\n\
                                        <button type="submit" data-item-id="' + bug.id + '" id="save-order-' + bug.id + '" class="col-sm-1 btn btn-primary save-order">Save</button>\n\
                                    </div>\n\
                                </form>\n\
                            </td>\n\
                        </tr>\n\
                        <tr class="collapsible-row-' + bug.id + '" style="display: none;">\n\
                            <td colspan="7">\n\
                                <p class=""><strong>' + bug.title + '</strong   ></p>\n\
                                <p class=""><strong>RayID: ' + bug.rayID + '</strong></p>\n\
                                <p class="" id="stackTrace-' + bug.id + '">' + bug.stackTrace.split("\n").join("<br>").split(" ").join("&nbsp;") + '</p>\n\
                            </td>\n\
                        </tr>'
                        );
                $("td").css("overflow","hidden");
                $("td").css("word-wrap","break-word");
                collapsible(bug.id);
                setDuplicate(bug.id);
                show(bug.id);
                setBugStatus(bug.id);
                setInputReadonly(bug.id);
                copyToPastebin(bug.id, bug.stackTrace);
                enableLink(bug.id);
            });
        });


    }
});