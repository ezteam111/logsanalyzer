angular
        .module('bugsBunny', ['ngMaterial', 'ngRoute'])
        .controller('AppCtrl', appCtrl)

function appCtrl($scope, $http, $location) {
    
    console.log($location.search());
    
    $http.get('http://localhost:8084/BugsBunny/api/apps').then(function (response) {
        $scope.apps = response.data.data;
    });
    $http.get('http://localhost:8084/BugsBunny/api/statuses').then(function (response) {
        $scope.statuses = response.data.data;
    });
}
