/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pb.bugzbunny.services;

import com.pb.bugzbunny.dto.Bug;
import com.pb.bugzbunny.dto.ScanResult;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Andrew Yatsenko <and1lays91@gmail.com>
 */
public class ElasticErrorSourceServiceIT {

    public ElasticErrorSourceServiceIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of findBugs method, of class ElasticErrorSourceService.
     */
    @Ignore
    @Test
    public void testFindBugs() throws Exception {
        System.out.println("ElasticErrorSourceService : findBugs()");
        String from = "";
        String to = "";
        int firstResult = 0;
        long maxResults = 0L;
        ElasticErrorSourceService instance = new ElasticErrorSourceService();
        ScanResult<Bug> expResult = null;
        ScanResult<Bug> result = instance.findBugs(from, to, firstResult, maxResults);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getElem method, of class ElasticErrorSourceService.
     */
    @Ignore
    @Test
    public void testGetElem() throws ParseException {
            System.out.println("* ElasticErrorSourceService: getElem()");
            String jsonString = "{\"@message\":\"414007509 INFO  o.a.s.s.m.AbstractValidatingSessionManager - Validating all active sessions...\n\",\"@timestamp\":\"2017-04-26T00:29:31.810Z\",\"@source\":\"maspay24\",\"@source_host\":\"VM-TARGET-2\",\"@fields\":{\"timestamp\":\"1493166571810\",\"host\":\"VM-TARGET-2\",\"dest\":\"default\",\"source\":\"maspay24\",\"route\":\"log\",\"rayID\":\"8534094f-bbd9-41ab-9906-32aac2c61b67\"}}";
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
            String row = "@fields.rayID";
            assertNotNull(new ElasticErrorSourceService().getElem(jsonObject, row));
            assertEquals("8534094f-bbd9-41ab-9906-32aac2c61b67", new ElasticErrorSourceService().getElem(jsonObject, row));
    }

    /**
     * Test of getElemFromFields method, of class ElasticErrorSourceService.
     */
}
